const os = require('os');
const path = require('path');

module.exports = {
  defaultDir: path.join(os.userInfo().homedir, '/ipfsbox'),
};
