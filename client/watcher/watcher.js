const chokidar = require('chokidar');
const properties = require('../common/properties.js');

// This module is used to tracking changes in IPFS Folder
class Watcher {
  static watchFile() {
    const watcher = chokidar.watch(properties.defaultDir, {
      persistent: true,
    });
    watcher.on('all', (event, path) => {
      console.log(event, path);
    });
  }
}

module.exports = Watcher;
