// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.
const electron = require('electron');
const path = require('path');
const BrowserWindow = electron.remote.BrowserWindow;
const signInBtn = document.getElementById('signInBtn');

signInBtn.addEventListener('click', function (event) {
    const modalPath = path.join('file://', __dirname, './homepage.html');
    let win = new BrowserWindow(
        {   width: 1200,
            height: 1000,
            frame: true, 
            webPreferences: {
                nodeIntegration: true
                },
        });
    win.on('close', function() { win = null });
    win.loadURL(modalPath);
    win.show();
});
 
